from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers 
from .models import Update 


def update_list(request):
    update_list = Update.objects.all()
    return render(request, 'update_list.html', {'object_list': update_list})
    
    
def updates_after(request, id):
    update_list = Update.objects.filter(pk__gt = id)
    json = serializers.serialize('json', update_list)
    response = HttpResponse()
    response['Content-Type'] = 'text/javascript'
    response.write(json)
    return response