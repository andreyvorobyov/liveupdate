from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.update_list),
    url(r'updates-after/(?P<id>\d+)/$', views.updates_after),
]    
